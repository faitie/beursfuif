﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Data;
using bCore;


namespace cBeursfuif
{
    public class cBeursfuif
    {
        /*------ PROP -----------------------------------*/
        public double[] currentPrice;
        public double[] minPrice;
        public double[] maxPrice;
        public double gewichtStijgen;
        public double gewichtDalen;
        public string[] itemNames;

        /*------ VARIABLES -----------------------------*/
        private int[] soldAmount;
        private Timer t1;
        private DataTable dt;
        private int timerWait;

        public cBeursfuif()
        {
            using (DataTable dt = new DataTable())
            {
                dt.Columns.Add("F-Key", typeof(string));
                dt.Columns.Add("Product naam", typeof(string));
                dt.Columns.Add("Tijd", typeof(DateTime));
                dt.Columns.Add("Aantal", typeof(int));
            }            
        }

        /*------ PUBLIC METHODS ------------------------*/
        public void start()
        {
            t1.Interval = (timerWait * 1000);
            t1.Tick += new EventHandler(t1_Tick);
            t1.Start();
        }

        public void checkInputKey(Keys _key)
        {
            switch(_key)
            {
                case Keys.F1:
                    break;
                case Keys.F2:
                    break;
                case Keys.F3:
                    break;
                case Keys.F4:
                    break;
                default:
                    break;
            }
        }

        /*------ PRIVATE METHODS -----------------------*/
        private void t1_Tick(object sender, EventArgs e)
        {
            t1.Stop();
            foreach (int i in soldAmount)
            {
                i = 0;
            }
        }
    
    }
}
