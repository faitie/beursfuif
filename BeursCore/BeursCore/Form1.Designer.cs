﻿namespace BeursCore
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRECALC = new System.Windows.Forms.Button();
            this.txtEEN = new System.Windows.Forms.TextBox();
            this.txtTWEE = new System.Windows.Forms.TextBox();
            this.txtDRIE = new System.Windows.Forms.TextBox();
            this.txtVIER = new System.Windows.Forms.TextBox();
            this.txtSOLD1 = new System.Windows.Forms.TextBox();
            this.txtSOLD2 = new System.Windows.Forms.TextBox();
            this.txtSOLD3 = new System.Windows.Forms.TextBox();
            this.txtSOLD4 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnRECALC
            // 
            this.btnRECALC.Location = new System.Drawing.Point(107, 212);
            this.btnRECALC.Name = "btnRECALC";
            this.btnRECALC.Size = new System.Drawing.Size(64, 27);
            this.btnRECALC.TabIndex = 0;
            this.btnRECALC.Text = "button1";
            this.btnRECALC.UseVisualStyleBackColor = true;
            this.btnRECALC.Click += new System.EventHandler(this.btnRECALC_Click);
            // 
            // txtEEN
            // 
            this.txtEEN.Location = new System.Drawing.Point(49, 33);
            this.txtEEN.Name = "txtEEN";
            this.txtEEN.Size = new System.Drawing.Size(100, 20);
            this.txtEEN.TabIndex = 1;
            this.txtEEN.Text = "1";
            // 
            // txtTWEE
            // 
            this.txtTWEE.Location = new System.Drawing.Point(49, 68);
            this.txtTWEE.Name = "txtTWEE";
            this.txtTWEE.Size = new System.Drawing.Size(100, 20);
            this.txtTWEE.TabIndex = 2;
            this.txtTWEE.Text = "1";
            // 
            // txtDRIE
            // 
            this.txtDRIE.Location = new System.Drawing.Point(49, 109);
            this.txtDRIE.Name = "txtDRIE";
            this.txtDRIE.Size = new System.Drawing.Size(100, 20);
            this.txtDRIE.TabIndex = 3;
            this.txtDRIE.Text = "1";
            // 
            // txtVIER
            // 
            this.txtVIER.Location = new System.Drawing.Point(49, 148);
            this.txtVIER.Name = "txtVIER";
            this.txtVIER.Size = new System.Drawing.Size(100, 20);
            this.txtVIER.TabIndex = 4;
            this.txtVIER.Text = "1";
            // 
            // txtSOLD1
            // 
            this.txtSOLD1.Location = new System.Drawing.Point(183, 33);
            this.txtSOLD1.Name = "txtSOLD1";
            this.txtSOLD1.Size = new System.Drawing.Size(43, 20);
            this.txtSOLD1.TabIndex = 5;
            this.txtSOLD1.Text = "3";
            // 
            // txtSOLD2
            // 
            this.txtSOLD2.Location = new System.Drawing.Point(183, 68);
            this.txtSOLD2.Name = "txtSOLD2";
            this.txtSOLD2.Size = new System.Drawing.Size(43, 20);
            this.txtSOLD2.TabIndex = 6;
            this.txtSOLD2.Text = "2";
            // 
            // txtSOLD3
            // 
            this.txtSOLD3.Location = new System.Drawing.Point(183, 109);
            this.txtSOLD3.Name = "txtSOLD3";
            this.txtSOLD3.Size = new System.Drawing.Size(43, 20);
            this.txtSOLD3.TabIndex = 7;
            this.txtSOLD3.Text = "1";
            // 
            // txtSOLD4
            // 
            this.txtSOLD4.Location = new System.Drawing.Point(183, 148);
            this.txtSOLD4.Name = "txtSOLD4";
            this.txtSOLD4.Size = new System.Drawing.Size(43, 20);
            this.txtSOLD4.TabIndex = 8;
            this.txtSOLD4.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 256);
            this.Controls.Add(this.txtSOLD4);
            this.Controls.Add(this.txtSOLD3);
            this.Controls.Add(this.txtSOLD2);
            this.Controls.Add(this.txtSOLD1);
            this.Controls.Add(this.txtVIER);
            this.Controls.Add(this.txtDRIE);
            this.Controls.Add(this.txtTWEE);
            this.Controls.Add(this.txtEEN);
            this.Controls.Add(this.btnRECALC);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRECALC;
        private System.Windows.Forms.TextBox txtEEN;
        private System.Windows.Forms.TextBox txtTWEE;
        private System.Windows.Forms.TextBox txtDRIE;
        private System.Windows.Forms.TextBox txtVIER;
        private System.Windows.Forms.TextBox txtSOLD1;
        private System.Windows.Forms.TextBox txtSOLD2;
        private System.Windows.Forms.TextBox txtSOLD3;
        private System.Windows.Forms.TextBox txtSOLD4;
    }
}

