﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace bCore
{
    public static class bCore
    {
        /*------ PROPERTIES ----------------------*/
                
        /*------ VARIABLES -----------------------*/
        private static double[] currentPrice;
        private static double[] minPrice;
        private static double[] maxPrice;
        private static int[] soldAmount;
        private static double gewichtStijgen;
        private static double gewichtDalen;

        private static double[] gemiddelde;
        private static double[] gemiddelde100;
        private static int som;
        private static double hoogste;
        private static double laagste;
        private static double verschil;
        private static double barema;
        private static int bascule;
        
        /*------ PUBLIC METHODS ------------------*/
        public static double[] recalcPrice(double[] _currentPrice, double[] _minPrice, double[] _maxPrice, int[] _soldAmount, double _gewichtStijgen, double _gewichtDalen, int _bascule)
        {
            int i = 0;

            gemiddelde = new double[_currentPrice.Length];
            gemiddelde100 = new double[_currentPrice.Length];

            currentPrice    = _currentPrice;
            minPrice        = _minPrice;
            maxPrice        = _maxPrice;
            soldAmount      = _soldAmount;
            gewichtStijgen  = _gewichtStijgen;
            gewichtDalen    = _gewichtDalen;
            bascule         = _bascule;

            som             = getSom(soldAmount);
            gemiddelde      = getGemiddelde(soldAmount, som);
            gemiddelde100   = getGemiddelde100(gemiddelde);
            hoogste         = getHoogste(gemiddelde100);
            laagste         = getLaagste(gemiddelde100);
            verschil        = getVerschil(hoogste, laagste);
            barema          = getBarema(verschil, bascule);

            if (hoogste != laagste)
            {
                foreach (double d in gemiddelde100)
                {
                    if ((d - laagste) > barema) /*prijs stijgen*/
                    {
                        currentPrice[i] = (currentPrice[i] + (gemiddelde[i] * gewichtStijgen));

                    }
                    else /*prijs dalen*/
                    {
                        currentPrice[i] = (currentPrice[i] - gewichtDalen);
                    }
                    /*min max controle*/
                    currentPrice[i] = checkMinMax(currentPrice[i], i);

                    i++;
                }
            }

            return currentPrice;
        }

        /*------ PRIVATE METHODS -----------------*/
        private static int getSom(int[] _soldAmount)
        {
            int _som = 0;

            foreach (int i in _soldAmount)
            {
                _som = _som + i;
            }

            return _som;
        }

        private static double[] getGemiddelde(int[] _soldAmount, int _som)
        {
            int i = 0;
            double[] _output = new double[_soldAmount.Length];

            foreach (int d in _soldAmount)
            {
                _output[i] = Convert.ToDouble((Convert.ToDouble(d) / Convert.ToDouble(_som)));
                i++;
            }

            return _output;
        }

        private static double[] getGemiddelde100(double[] _gemiddelde)
        {
            int i = 0;
            double[] _output = new double[_gemiddelde.Length];

            foreach (double d in _gemiddelde)
            {
                _output[i] = Math.Round((d * 100), 2);
                i++;
            }
            return _output;
        }

        private static double getHoogste(double[] _gemiddelde100)
        {
            double _hoogste = 0;

            foreach (double d in _gemiddelde100)
            {
                if (d > _hoogste) { _hoogste = d; }
            }
            return _hoogste;
        }

        private static double getLaagste(double[] _gemiddelde100)
        {
            double _laagste = 1000;

            foreach (double d in _gemiddelde100)
            {
                if (d < _laagste) { _laagste = d; }
            }
            return _laagste;
        }

        private static double getVerschil(double _hoogste, double _laagste)
        {
            double _verschil;

            _verschil = _hoogste - _laagste;

            return _verschil;
        }

        private static double getBarema(double _verschil, int _bascule)
        {
            double _barema;

            _barema = (_verschil / 100) * _bascule;

            return _barema;
        }

        private static double checkMinMax(double itemPrice, int itemCount)
        {
            double _output = itemPrice;

            if (itemPrice > maxPrice[itemCount]) { _output = maxPrice[itemCount]; }
            if (itemPrice < minPrice[itemCount]) { _output = minPrice[itemCount]; }

            return _output;
        }
    
    }
}
