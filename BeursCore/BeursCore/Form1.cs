﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using bCore;

namespace BeursCore
{
    public partial class Form1 : Form
    {
        double[] currentPrice = null;
        double[] minPrice = null;
        double[] maxPrice = null;
        int[] soldAmount = null;
        double gewichtStijgen = 0;
        double gewichtDalen = 0;


        public Form1()
        {
            InitializeComponent();
            minPrice = new double[4];
            maxPrice = new double[4];
            currentPrice = new double[4];
            soldAmount = new int[4];

            for (int i = 0; i < 4; i++)
            {
                minPrice[i] = 0.8;
                maxPrice[i] = 3.0;
            }
        }

        private void btnRECALC_Click(object sender, EventArgs e)
        {
            currentPrice[0] = Convert.ToDouble(txtEEN.Text);
            currentPrice[1] = Convert.ToDouble(txtTWEE.Text);
            currentPrice[2] = Convert.ToDouble(txtDRIE.Text);
            currentPrice[3] = Convert.ToDouble(txtVIER.Text);

            soldAmount[0]   = Convert.ToInt16(txtSOLD1.Text);
            soldAmount[1]   = Convert.ToInt16(txtSOLD2.Text);
            soldAmount[2]   = Convert.ToInt16(txtSOLD3.Text);
            soldAmount[3]   = Convert.ToInt16(txtSOLD4.Text);

            currentPrice    = bCore.bCore.recalcPrice(currentPrice, minPrice, maxPrice, soldAmount, 0.2, 0.02, 50);

            txtEEN.Text     = currentPrice[0].ToString();
            txtTWEE.Text    = currentPrice[1].ToString();
            txtDRIE.Text    = currentPrice[2].ToString();
            txtVIER.Text    = currentPrice[3].ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
