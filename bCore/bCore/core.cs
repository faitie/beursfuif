﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace bCore
{
    public static class bCore
    {
        /*------ PROPERTIES ----------------------*/
        public static string    cError
        {
            private set { cError = value; }
        }
        
        /*------ VARIABLES -----------------------*/
        private double[]    currentPrice;
        private double[]    minPrice;
        private double[]    maxPrice;
        private int[]       soldAmount;
        private double      gewichtStijgen;
        private double      gewichtDalen;

        private double[] gemiddelde;
        private double[] gemiddelde100;
        private int som;
        private double hoogste;
        private double laagste;
        private double verschil;
        private double barema;
        private int bascule;
        
        /*------ PUBLIC METHODS ------------------*/
        public double[] recalcPrice(double[] _currentPrice, double[] _minPrice, double[] _maxPrice, int[] _soldAmount, double _gewichtStijgen, double _gewichtDalen, int _bascule)
        {
            int i = 0;

            currentPrice    = _currentPrice;
            minPrice        = _minPrice;
            maxPrice        = _maxPrice;
            soldAmount      = _soldAmount;
            gewichtStijgen  = _gewichtStijgen;
            gewichtDalen    = _gewichtDalen;
            bascule         = _bascule;

            som             = getSom(soldAmount);
            gemiddelde      = getGemiddelde(currentPrice, som);
            gemiddelde100   = getGemiddelde100(gemiddelde);
            hoogste         = getHoogste(gemiddelde100);
            laagste         = getLaagste(gemiddelde100);
            verschil        = getVerschil(hoogste, laagste);
            barema          = getBarema(verschil, bascule);

            foreach (double d in gemiddelde100)
            {
                if ((d - laagste) > barema) /*prijs stijgen*/
                {
                    currentPrice[i] = (currentPrice[i] + (gemiddelde[i] * gewichtStijgen));
                    
                }
                else /*prijs dalen*/
                {
                    currentPrice[i] = (currentPrice[i] - gewichtDalen);
                }
                /*min max controle*/
                currentPrice[i] = checkMinMax(currentPrice[i],i);

                i++;
            }

            return currentPrice;
        }

        /*------ PRIVATE METHODS -----------------*/
        private bool checkRequired()
        {
            bool check = true;

            foreach (double d in items)
            {
                if (d == 0 || d == null) { check = false; }
            }
            return check;
        }

        private int getSom(int[] _soldAmount)
        {
            int _som = 0;

            foreach (int i in _soldAmount)
            {
                _som = _som + i;
            }

            return _som;
        }

        private double[] getGemiddelde(double[] _soldAmount, int _som)
        {
            int i = 0;
            double[] _output = null;

            foreach (double d in _soldAmount)
            {
                _output[i] = (d / _som);
                i++;
            }

            return _output;
        }

        private double[] getGemiddelde100(double[] _gemiddelde)
        {
            int i = 0;
            double[] _output = null;

            foreach (double d in _gemiddelde)
            {
                _output[i] = Math.Round((d * 100), 2);
                i++;
            }
            return _output;
        }

        private double getHoogste(double[] _gemiddelde100)
        {
            double _hoogste = 0;

            foreach (double d in _gemiddelde100)
            {
                if (d > _hoogste) { _hoogste = d; }
            }
            return _hoogste;
        }

        private double getLaagste(double[] _gemiddelde100)
        {
            double _laagste = 1000;

            foreach (double d in _gemiddelde100)
            {
                if (d < _laagste) { _laagste = d; }
            }
            return _laagste;
        }

        private double getVerschil(double _hoogste, double _laagste)
        {
            double _verschil;

            verschil = _hoogste - _laagste;

            return verschil;
        }

        private double getBarema(double _verschil, int _bascule)
        {
            double _barema;

            _barema = (_verschil / 100) * _bascule;

            return _barema;
        }

        private double checkMinMax(double itemPrice, int itemCount)
        {
            double _output = 0;

            if (itemPrice > maxPrice[itemCount]) { _output = maxPrice[itemCount]; }
            if (itemPrice < minPrice[itemCount]) { _output = minPrice[itemCount]; }

            return _output;
        }
    
    }
}
